<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Unit;

class Units extends Component
{
    public $units, $unit_id, $name, $description;
    public $isOpen = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function render()
    {
        $this->units = Unit::all();
        return view('admin.unit.units')->layout('layouts.admin');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->isOpen = true;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->name = '';
        $this->description = '';
        $this->unit_id = '';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $this->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        Unit::updateOrCreate(['id' => $this->unit_id], [
            'name' => $this->name,
            'description' => $this->description,
        ]);

        session()->flash('message',
            $this->unit_id ? 'Unit Updated Successfully.' : 'Unit Created Successfully.');

        $this->closeModal();
        $this->resetInputFields();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $unit = Unit::findOrFail($id);
        $this->unit_id = $id;
        $this->name = $unit->name;
        $this->description = $unit->description;

        $this->openModal();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Unit::find($id)->delete();
        session()->flash('message', 'Unit Deleted Successfully.');
    }
}
