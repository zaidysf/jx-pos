<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Navigation extends Component
{
    /**
     * The component's listeners.
     *
     * @var array
     */
     protected $listeners = [
        'refresh-admin-navigation' => '$refresh',
    ];

    /**
     * Render the component.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        return view('admin.navigation');
    }
}
