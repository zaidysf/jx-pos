<div align="center">
  <img src="https://juraganx.com/_next/static/images/logo-dark-b74c4a97280f1e097b9b8d29c27239da.png" alt="Logo" width="250" height="60">
  <div align="center">
    <br/>
    <a href="https://gitlab.com/zaidysf/jx-pos/-/commits/master"><img src="https://img.shields.io/gitlab/pipeline/zaidysf/jx-pos" /></a>
    <a href="https://gitlab.com/zaidysf/jx-pos/-/starrers"><img src="https://flat.badgen.net/gitlab/stars/zaidysf/jx-pos" /></a>
    <a href="https://gitlab.com/zaidysf/jx-pos/-/forks"><img src="https://flat.badgen.net/gitlab/forks/zaidysf/jx-pos" /></a>
    <a href="https://gitlab.com/zaidysf/jx-pos/issues"><img src="https://flat.badgen.net/gitlab/issues/zaidysf/jx-pos" /></a>
    <a href="https://gitlab.com/zaidysf/jx-pos/-/branches"><img src="https://flat.badgen.net/gitlab/branches/zaidysf/jx-pos" /></a>
    <a href="https://gitlab.com/zaidysf/jx-pos/-/tags"><img src="https://flat.badgen.net/gitlab/tags/zaidysf/jx-pos" /></a>
    <a href="https://gitlab.com/zaidysf/jx-pos/-/commits/master"><img src="https://flat.badgen.net/gitlab/last-commit/zaidysf/jx-pos" /></a>
    <a href="https://juraganx.com"><img src="https://flat.badgen.net/badge/support/juraganx/3988FB"/></a>
  </div>
  <div align="center">
    JX POS by JuraganX
  </div>
</div>


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This project require [Laravel 8](https://laravel.com/docs/8.x) 's server requirement

### Installing

Get composer package updated

```
composer install
```

Copy and rename .env.example

```
cp .env.example .env
```

Generate application key

```
php artisan storage:link
```

Get node package updated

```
npm install
```

Run all Mix tasks

```
npm run dev
```

Link storage (for uploading files)

```
php artisan storage:link
```

Migrate the database

```
php artisan migrate
```

Finally, run the local server

```
php artisan serve
```

## Running the tests

// To Do

### Break down into end to end tests

// To Do

### And coding style tests

// To Do

## Deployment

// To Do

## Built With

* [Laravel](https://www.laravel.com/) - The web framework used
* [Composer](https://getcomposer.org) - Depedency Manager for PHP
* [NPM](https://composer.org) - Node Package Manager
* [Jetstream](https://jetstream.laravel.com/) - Application Scaffolding for Laravel
* [Livewire](https://laravel-livewire.com/) - Full-stack Framework for Laravel that makes dynamic interfaces

## Contributing

// To Do

## Versioning

// To Do 

## Authors

* **Zaid** - *JuraganX Dev* - [JuraganX](https://juraganx.com)
* **Witan** - *JuraganX Dev* - [JuraganX](https://juraganx.com)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

// To Do

## Acknowledgments

// To Do

