<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuAddonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_addons', function (Blueprint $table) {
            $table->id();
            $table->integer('menu_id')->index();
            $table->tinyInteger('reference_type')->default(1)->comment('Fill 1 if using product, 2 if using material, fill 3 if using package');
            $table->integer('reference_id');
            $table->tinyInteger('status')->default(1)->comment('Fill 0 if not active, 1 if active, 2 if out of stock');
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_addons');
    }
}
