<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->id();
            $table->integer('resto_id')->index();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->integer('city_id')->index();
            $table->string('longitude')->index();
            $table->string('latitude')->index();
            $table->text('logo_path')->nullable();
            $table->date('established_date')->nullable();
            $table->string('instagram')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('facebook')->nullable();
            $table->string('website')->nullable();
            $table->integer('leader_id')->default(0)->index();
            $table->tinyInteger('is_main')->default(0);
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
