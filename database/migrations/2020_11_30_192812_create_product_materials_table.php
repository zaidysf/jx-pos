<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_materials', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->index();
            $table->integer('material_id')->index()->comment('Fill 0 if is_other_material is true');
            $table->text('photo_path')->nullable();
            $table->float('qty')->default(0);
            $table->float('cogs')->nullable()->comment('Filled if is_other_material is true (such as Water, Gas, etc.)');
            $table->string('description')->nullable()->comment('Fill 0 if is_other_material is true');
            $table->tinyInteger('is_other_material')->default(0);
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_materials');
    }
}
