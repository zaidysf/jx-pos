<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->id();
            $table->integer('branch_id')->index();
            $table->text('photo_path')->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->float('cogs')->default(0);
            $table->float('selling_price')->default(0);
            $table->float('qty')->default(0);
            $table->integer('unit_id');
            $table->tinyInteger('is_expirable')->default(1);
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
