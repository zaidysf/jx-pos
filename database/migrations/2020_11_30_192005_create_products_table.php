<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('branch_id')->index();
            $table->integer('product_id')->default(0)->index()->comment("Fill 0 if product is not mixed by other products");
            $table->text('photo_path')->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->float('cogs')->comment("Filled automatically by sum product_materials cogs");
            $table->float('qty')->nullable();
            $table->float('selling_price')->default(0);
            $table->integer('unit_id');
            $table->tinyInteger('is_expirable')->default(1);
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
